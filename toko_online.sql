-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 14, 2019 at 01:58 PM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_online`
--

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `id_berita` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `jenis_berita` varchar(255) NOT NULL,
  `judul_berita` varchar(255) NOT NULL,
  `slug_berita` varchar(255) NOT NULL,
  `keywords` text,
  `status_berita` varchar(255) NOT NULL,
  `keterangan` text NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `tanggal_post` datetime NOT NULL,
  `tanggal_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gambar`
--

CREATE TABLE `gambar` (
  `id_gambar` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `judul_gambar` varchar(255) DEFAULT NULL,
  `gambar` varchar(255) NOT NULL,
  `tanggal_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gambar`
--

INSERT INTO `gambar` (`id_gambar`, `id_produk`, `judul_gambar`, `gambar`, `tanggal_update`) VALUES
(3, 10, 'tes dulu', 'tes.png', '2019-05-07 03:34:10'),
(5, 10, 'coba kedua ketiga', 'users1.jpg', '2019-05-07 03:58:44');

-- --------------------------------------------------------

--
-- Table structure for table `header_transaksi`
--

CREATE TABLE `header_transaksi` (
  `id_header_transaksi` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `nama_pelanggan` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telepon` varchar(255) DEFAULT NULL,
  `alamat` varchar(400) DEFAULT NULL,
  `kode_transaksi` varchar(255) NOT NULL,
  `tanggal_transaksi` datetime NOT NULL,
  `jumlah_transaksi` int(11) NOT NULL,
  `status_bayar` varchar(20) NOT NULL,
  `jumlah_bayar` int(11) DEFAULT NULL,
  `rekening_pembayaran` varchar(255) DEFAULT NULL,
  `rekening_pelanggan` varchar(255) DEFAULT NULL,
  `bukti_bayar` varchar(255) DEFAULT NULL,
  `tanggal_post` datetime NOT NULL,
  `tanggal_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `header_transaksi`
--

INSERT INTO `header_transaksi` (`id_header_transaksi`, `id_user`, `id_pelanggan`, `nama_pelanggan`, `email`, `telepon`, `alamat`, `kode_transaksi`, `tanggal_transaksi`, `jumlah_transaksi`, `status_bayar`, `jumlah_bayar`, `rekening_pembayaran`, `rekening_pelanggan`, `bukti_bayar`, `tanggal_post`, `tanggal_update`) VALUES
(1, 0, 6, 'yong', 'yong@gamil.com', '08763458', 'mataram', '15052019SXM8TRMR', '2019-05-15 00:00:00', 4000, 'belum', NULL, NULL, NULL, NULL, '2019-05-15 03:12:00', '2019-05-15 01:12:00'),
(2, 0, 6, 'yong', 'yong@gamil.com', '08763458', 'mataram', '15052019YCYEDPIK', '2019-05-15 00:00:00', 129000, 'belum', NULL, NULL, NULL, NULL, '2019-05-15 03:18:32', '2019-05-15 01:18:32'),
(3, 0, 6, 'yong', 'yong@gamil.com', '08763458', 'mataram', '15052019EXHQNEAZ', '2019-05-15 00:00:00', 20000, 'belum', NULL, NULL, NULL, NULL, '2019-05-15 08:22:57', '2019-05-15 06:22:57'),
(4, 0, 6, 'yong', 'yong@gamil.com', '08763458', 'mataram', '15052019EZCMDLES', '2019-05-15 00:00:00', 24000, 'belum', NULL, NULL, NULL, NULL, '2019-05-15 10:59:36', '2019-05-15 08:59:36'),
(5, 0, 6, 'yong cakeps', 'yong@gamil.com', '08763458', 'mataram', '150520191CAXXWAQ', '2019-05-15 00:00:00', 20000, 'belum', NULL, NULL, NULL, NULL, '2019-05-15 11:45:33', '2019-05-15 09:45:33'),
(6, 0, 6, 'yong cakeps', 'yong@gamil.com', '08763458', 'mataram', '15052019TYWOILNJ', '2019-05-15 00:00:00', 440000, 'belum', NULL, NULL, NULL, NULL, '2019-05-15 16:27:56', '2019-05-15 14:27:56'),
(7, 0, 6, 'yong cakeps', 'yong@gamil.com', '08763458', 'mataram', '15052019ZD3CNLVM', '2019-05-15 00:00:00', 220000, 'belum', NULL, NULL, NULL, NULL, '2019-05-15 16:50:30', '2019-05-15 14:50:30'),
(8, 0, 7, 'muh', 'a@gmail.com', '8888', 'kjkjh', '13062019F5SUD4LN', '2019-06-13 00:00:00', 120000, 'belum', NULL, NULL, NULL, NULL, '2019-06-13 03:51:52', '2019-06-13 01:51:52');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL,
  `slug_kategori` varchar(255) NOT NULL,
  `nama_kategori` varchar(255) NOT NULL,
  `urutan` int(11) DEFAULT NULL,
  `tanggal_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `slug_kategori`, `nama_kategori`, `urutan`, `tanggal_update`) VALUES
(1, 'kursi-tinggi', 'Kursi Tinggi', 1, '2019-06-14 11:54:32'),
(3, 'minuman', 'Minuman', 3, '2019-06-14 11:54:16'),
(4, 'makanan', 'Makanan', 4, '2019-06-14 11:53:57'),
(5, 'kereta-bayi', 'Kereta Bayi', 2, '2019-06-14 11:54:51'),
(6, 'perlengkapan-minum', 'Perlengkapan Minum', 5, '2019-06-14 11:55:09');

-- --------------------------------------------------------

--
-- Table structure for table `konfigurasi`
--

CREATE TABLE `konfigurasi` (
  `id_konfigurasi` int(11) NOT NULL,
  `namaweb` varchar(255) NOT NULL,
  `tagline` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `keywords` text,
  `metatext` text,
  `telepon` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `instagram` varchar(255) DEFAULT NULL,
  `deskripsi` text,
  `logo` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `rekening_pembayaran` varchar(255) DEFAULT NULL,
  `tanggal_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konfigurasi`
--

INSERT INTO `konfigurasi` (`id_konfigurasi`, `namaweb`, `tagline`, `email`, `website`, `keywords`, `metatext`, `telepon`, `alamat`, `facebook`, `instagram`, `deskripsi`, `logo`, `icon`, `rekening_pembayaran`, `tanggal_update`) VALUES
(1, 'Tokobalonku-Shop', 'spesialis penjualan terlengkap', 'nanda@gmail.com', 'http://tokonanda.com', '						tokobalonku shop penjualan terlengkap									', '						Tokobalonku Shop										', '087545666565', '				Lombok							', 'https://web.facebook.com/nanda.l.humairoh', 'https://www.instagram.com/tomika_nanda/', '										website penjualan								', 'logo21.jpeg', 'logo2.jpeg', '										ok												', '2019-06-14 06:11:23');

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id_pelanggan` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `status_pelanggan` varchar(20) NOT NULL,
  `nama_pelanggan` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(64) NOT NULL,
  `telepon` varchar(50) DEFAULT NULL,
  `alamat` varchar(300) DEFAULT NULL,
  `tanggal_daftar` datetime NOT NULL,
  `tanggal_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`id_pelanggan`, `id_user`, `status_pelanggan`, `nama_pelanggan`, `email`, `password`, `telepon`, `alamat`, `tanggal_daftar`, `tanggal_update`) VALUES
(4, 0, 'pending', 'yongs', 'youngs@gmail.com', 'bf234dad7b79b7b5998221304f7d92e990a45f57', '2432', 'mataram', '2019-05-14 18:33:47', '2019-05-14 16:33:47'),
(5, 0, 'pending', 'yongss', 'y@gmail.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '2432', 'mataram', '2019-05-14 19:08:01', '2019-05-14 17:08:01'),
(6, 0, 'pending', 'yong cakeps', 'yong@gamil.com', 'f869101330fe5fdb4f871dbdaf6c06d091a80767', '08763458', 'mataram', '2019-05-15 02:45:26', '2019-05-15 09:40:45'),
(7, 0, 'pending', 'muh', 'a@gmail.com', '356a192b7913b04c54574d18c28d46e6395428ab', '8888', 'kjkjh', '2019-06-13 03:51:32', '2019-06-13 01:51:32');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `kode_produk` varchar(20) NOT NULL,
  `nama_produk` varchar(255) NOT NULL,
  `slug_produk` varchar(255) NOT NULL,
  `keterangan` text NOT NULL,
  `keywords` text,
  `harga` int(11) NOT NULL,
  `stok` int(11) DEFAULT NULL,
  `gambar` varchar(255) NOT NULL,
  `berat` float DEFAULT NULL,
  `ukuran` varchar(255) DEFAULT NULL,
  `status_produk` varchar(50) NOT NULL,
  `tanggal_post` datetime NOT NULL,
  `tanggal_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `id_user`, `id_kategori`, `kode_produk`, `nama_produk`, `slug_produk`, `keterangan`, `keywords`, `harga`, `stok`, `gambar`, `berat`, `ukuran`, `status_produk`, `tanggal_post`, `tanggal_update`) VALUES
(19, 15, 6, 'a', 'a', 'a-a', '<p>sdfsd</p>\r\n', '					sds	', 750000, 3, '201803011912311.png', 4, 'ds', 'Publish', '2019-06-14 13:57:11', '2019-06-14 11:57:11');

-- --------------------------------------------------------

--
-- Table structure for table `rekening`
--

CREATE TABLE `rekening` (
  `id_rekening` int(11) NOT NULL,
  `nama_bank` varchar(255) NOT NULL,
  `nomor_rekening` varchar(25) NOT NULL,
  `nama_pemilik` varchar(255) NOT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `tanggal_post` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `kode_transaksi` varchar(255) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `total_harga` int(11) NOT NULL,
  `tanggal_transaksi` datetime NOT NULL,
  `tanggal_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_user`, `id_pelanggan`, `kode_transaksi`, `id_produk`, `harga`, `jumlah`, `total_harga`, `tanggal_transaksi`, `tanggal_update`) VALUES
(1, 0, 6, '15052019SXM8TRMR', 12, 4000, 1, 4000, '2019-05-15 00:00:00', '2019-05-15 01:12:00'),
(2, 0, 6, '15052019YCYEDPIK', 15, 20000, 1, 20000, '2019-05-15 00:00:00', '2019-05-15 01:18:32'),
(3, 0, 6, '15052019YCYEDPIK', 14, 100000, 1, 100000, '2019-05-15 00:00:00', '2019-05-15 01:18:33'),
(4, 0, 6, '15052019YCYEDPIK', 16, 4000, 1, 4000, '2019-05-15 00:00:00', '2019-05-15 01:18:33'),
(5, 0, 6, '15052019YCYEDPIK', 11, 5000, 1, 5000, '2019-05-15 00:00:00', '2019-05-15 01:18:33'),
(6, 0, 6, '15052019EXHQNEAZ', 15, 20000, 1, 20000, '2019-05-15 00:00:00', '2019-05-15 06:22:57'),
(7, 0, 6, '15052019EZCMDLES', 16, 4000, 1, 4000, '2019-05-15 00:00:00', '2019-05-15 08:59:36'),
(8, 0, 6, '15052019EZCMDLES', 15, 20000, 1, 20000, '2019-05-15 00:00:00', '2019-05-15 08:59:36'),
(9, 0, 6, '150520191CAXXWAQ', 15, 20000, 1, 20000, '2019-05-15 00:00:00', '2019-05-15 09:45:33'),
(10, 0, 6, '15052019TYWOILNJ', 18, 140000, 1, 140000, '2019-05-15 00:00:00', '2019-05-15 14:27:56'),
(11, 0, 6, '15052019TYWOILNJ', 12, 150000, 1, 150000, '2019-05-15 00:00:00', '2019-05-15 14:27:57'),
(12, 0, 6, '15052019TYWOILNJ', 16, 150000, 1, 150000, '2019-05-15 00:00:00', '2019-05-15 14:27:57'),
(13, 0, 6, '15052019ZD3CNLVM', 11, 20000, 1, 20000, '2019-05-15 00:00:00', '2019-05-15 14:50:30'),
(14, 0, 6, '15052019ZD3CNLVM', 15, 200000, 1, 200000, '2019-05-15 00:00:00', '2019-05-15 14:50:30'),
(15, 0, 7, '13062019F5SUD4LN', 11, 20000, 1, 20000, '2019-06-13 00:00:00', '2019-06-13 01:51:52'),
(16, 0, 7, '13062019F5SUD4LN', 10, 100000, 1, 100000, '2019-06-13 00:00:00', '2019-06-13 01:51:52');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `akses_level` varchar(50) NOT NULL,
  `tanggal_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `nama`, `email`, `username`, `password`, `akses_level`, `tanggal_update`) VALUES
(15, 'Admin', 'sfdsa@gmail.com', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Admin', '2019-06-14 05:28:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `gambar`
--
ALTER TABLE `gambar`
  ADD PRIMARY KEY (`id_gambar`);

--
-- Indexes for table `header_transaksi`
--
ALTER TABLE `header_transaksi`
  ADD PRIMARY KEY (`id_header_transaksi`),
  ADD UNIQUE KEY `kode_transaksi` (`kode_transaksi`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `konfigurasi`
--
ALTER TABLE `konfigurasi`
  ADD PRIMARY KEY (`id_konfigurasi`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id_pelanggan`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`),
  ADD UNIQUE KEY `kode_produk` (`kode_produk`);

--
-- Indexes for table `rekening`
--
ALTER TABLE `rekening`
  ADD PRIMARY KEY (`id_rekening`),
  ADD UNIQUE KEY `nomor_rekening` (`nomor_rekening`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gambar`
--
ALTER TABLE `gambar`
  MODIFY `id_gambar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `header_transaksi`
--
ALTER TABLE `header_transaksi`
  MODIFY `id_header_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `konfigurasi`
--
ALTER TABLE `konfigurasi`
  MODIFY `id_konfigurasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `rekening`
--
ALTER TABLE `rekening`
  MODIFY `id_rekening` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
